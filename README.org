# artefacts
Artefacts (Images, Interactive artefacts) and realization
catalog of the experiment are pushed here.

Refer instructions [[https://gitlab.com/vlead-projects/experiments/on-boarding/blob/master/src/artefacts-realization.org][here]]
+ to view the structure of the repo
+ to run the artefacts server

Refer instructions [[https://gitlab.com/vlead-projects/experiments/on-boarding/blob/master/src/exp-dev-process.org][here]] to understand how the realization catalog
is built for an experiment.
